import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_ssh(host):
    x = host.ansible("raw", "net start", check=False)["stdout"]
    assert "OpenSSH SSH Server" in x
