# ics-ans-role-win-ssh

Ansible role to install win-ssh.

Downloads a compressed zip file from official GitHub project.
Version is declared in defaults/main.

## Role Variables

```yaml
win_sshserver_version: "v8.9.1.0p1-Beta"
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-win-ssh
```

## License

BSD 2-clause
